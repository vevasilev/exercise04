import java.util.Arrays;

public class Exercise04 {
    public static void main(String[] args) {
        int[] array = new int[8];

        int maxValue = 10;
        for (int i = 0; i < array.length; i++) {
            array[i] = (int) (Math.random() * maxValue) + 1;
        }
        System.out.println(Arrays.toString(array));

        System.out.println(isIncreasingSequence(array));

        for (int i = 1; i < array.length; i += 2) {
            array[i] = 0;
        }
        System.out.println(Arrays.toString(array));
    }

    private static String isIncreasingSequence(int[] array) {
        for (int i = 1; i < array.length; i++) {
            if (array[i] <= array[i - 1]) {
                return "The array is not a strictly increasing sequence";
            }
        }
        return "The array is a strictly increasing sequence";
    }
}
